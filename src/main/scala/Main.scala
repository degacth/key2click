object Main extends App {
  try {
    AppManager.create()
  } catch {
    case e: Exception => e.printStackTrace()
  }

  GlobalKeyListener.onUserStart {
    AppManager.startKey2Click()
  }
}
