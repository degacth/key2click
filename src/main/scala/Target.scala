import java.awt.{Color, Font, Graphics}

class Target(val name: String, val pos: (Int, Int), val size: (Int, Int)) {
  def draw(g: Graphics): Unit = {
    g.setColor(Color.LIGHT_GRAY)
    g.drawRect(pos._1, pos._2, size._1, size._2)

    g.setColor(Color.DARK_GRAY)
    g.drawRect(pos._1 + 1, pos._2 + 1, size._1 - 2, size._2 - 2)

    val baseFontSize = 16
    val center = (pos._1 + size._1 / 2) -> (pos._2 + size._2 / 2)
    this.drawName(g, this.name, center, Color.DARK_GRAY, baseFontSize + 3)
    this.drawName(g, this.name, center, Color.WHITE, baseFontSize)
  }

  def createTargets(items: (Int, Int)): Map[String, Target] = {
    val targetNames = 'a' to 'z'
    val (w, h) = (size._1 / items._1) -> (size._2 / items._2)
    val targets = for (y <- 0 until items._1; x <- 0 until items._2) yield new Target(
      targetNames(y * items._2 + x).toString, (x * w + pos._1) -> (y * h + pos._2), w -> h
    )
    targets.map(t => t.name -> t).toMap
  }

  private def drawName(g: Graphics, name: String, pos: (Int, Int), color: Color, fontSize: Int): Unit = {
    val fontSmall = new Font(Font.MONOSPACED, Font.PLAIN, fontSize)
    g.setFont(fontSmall)
    val fontMetrics = g.getFontMetrics

    g.setColor(color)
    g.drawString(name, pos._1 - fontMetrics.getAscent / 2, pos._2 + fontMetrics.getAscent / 2)
  }
}
