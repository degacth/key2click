import java.awt.event.InputEvent
import java.awt.image.BufferedImage
import java.awt.{Graphics, GraphicsDevice, Rectangle, Robot}

class ScreenManager(devices: Array[GraphicsDevice]) {
  private val robotOnScreens: Array[RobotScreen] = devices.map(new RobotScreen(_))
  private var targets: Map[String, Target] = Map()
  private val targetsSize = 5 -> 5

  def getTarget: String => Option[Target] = targets.get _

  def setScreensImages(): Unit = robotOnScreens.foreach(_.saveScreenShot())

  def drawPanel(device: GraphicsDevice, canvas: Graphics, target: Option[Target]): Unit = {
    findRobotOnScreen(device).foreach { rs =>
      rs.screenImage.foreach { image =>
        canvas.drawImage(image, 0, 0, null)
      }

      targets = target.getOrElse {
        new Target("", 0 -> 0, rs.getSize)
      }.createTargets(targetsSize)

      targets.foreach { case (_: String, target: Target) => target.draw(canvas) }
    }
  }

  def emitClick(t: Target, device: GraphicsDevice): Unit = {
    val (x, y) = (t.pos._1 + t.size._1 / 2) -> (t.pos._2 + t.size._2 / 2)
    findRobotOnScreen(device).foreach { rs =>
      val robot = rs.robot
      val bounds = device.getDefaultConfiguration.getBounds
      robot.mouseMove(x + bounds.x, y + bounds.y)
      val keyMask = InputEvent.BUTTON1_DOWN_MASK
      robot.delay(300)
      robot.mousePress(keyMask)
      robot.mouseRelease(keyMask)
    }
  }

  private def findRobotOnScreen(device: GraphicsDevice): Option[RobotScreen] = robotOnScreens.find(_.device == device)
}

sealed class RobotScreen(val device: GraphicsDevice) {
  val robot = new Robot(device)
  var screenImage: Option[BufferedImage] = None

  def saveScreenShot(): Unit = {
    screenImage = Some(robot.createScreenCapture(device.getDefaultConfiguration.getBounds))
  }

  def getSize: (Int, Int) = {
    val displayMode = device.getDisplayMode
    displayMode.getWidth -> displayMode.getHeight
  }
}
