import java.awt._
import java.awt.event.{InputEvent, KeyAdapter, KeyEvent}
import javax.swing._

class Frame extends JFrame {
  private val screenManager = new ScreenManager(
    GraphicsEnvironment.getLocalGraphicsEnvironment.getScreenDevices
  )
  private var selectedTarget: Option[Target] = None

  private val pane = new JPanel {
    override def paintComponent(g: Graphics): Unit = {
      super.paintComponent(g)
      screenManager.drawPanel(getGraphicsConfiguration.getDevice, g, selectedTarget)
    }
  }

  add(pane)
  addKeyListener(new KeyAdapter {
    override def keyPressed(e: KeyEvent): Unit = onPanelKeyPressed(e)
  })

  setFocusable(true)
  setTitle("key 2 click")
  setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)
  setUndecorated(true)

  def startAction(): Unit = {
    screenManager.setScreensImages()
    showWindow()
  }

  private def showWindow(): Unit = {
    setVisible(true)
    getGraphicsConfiguration.getDevice.setFullScreenWindow(this)
  }

  private def onPanelKeyPressed(keyEvent: KeyEvent): Unit = {
    val key = KeyEvent.getKeyText(keyEvent.getKeyCode).toLowerCase
    screenManager.getTarget(key).foreach { t =>
      if ((keyEvent.getModifiersEx & InputEvent.SHIFT_DOWN_MASK) > 0) {
        setVisible(false)
        screenManager.emitClick(t, getGraphicsConfiguration.getDevice)
        selectedTarget = None
      } else {
        selectedTarget = Some(t)
        pane.repaint()
      }
    }
  }
}
