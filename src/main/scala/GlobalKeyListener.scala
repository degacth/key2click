import java.util.logging.{Level, Logger}

import org.jnativehook.{GlobalScreen, NativeHookException}
import org.jnativehook.keyboard.NativeKeyEvent
import org.jnativehook.keyboard.NativeKeyAdapter

import scala.collection.mutable

object GlobalKeyListener {
  private val logger = Logger.getLogger(classOf[GlobalScreen].getPackage.getName)
  logger.setLevel(Level.WARNING)
  logger.setUseParentHandlers(false)

  try
    GlobalScreen.registerNativeHook()
  catch {
    case ex: NativeHookException =>
      System.err.println("There was a problem registering the native hook.")
      System.err.println(ex.getMessage)
      System.exit(1)
  }

  private val hotKeys = List(NativeKeyEvent.VC_ALT, NativeKeyEvent.VC_CONTROL, NativeKeyEvent.VC_SEMICOLON)
  private val pressedHotKeys: mutable.Map[Int, Boolean] = mutable.Map(hotKeys.map(key => key -> false).toMap.toSeq: _*)

  def onUserStart(f: => Unit): Unit = {
    GlobalScreen.addNativeKeyListener(new NativeKeyAdapter {
      override def nativeKeyPressed(nativeKeyEvent: NativeKeyEvent): Unit = {
        if (!isInHotKeys(nativeKeyEvent.getKeyCode)) return
        setHotKeyPressed(nativeKeyEvent.getKeyCode, value = true)

        if (isAllKeyPressed) f
      }

      override def nativeKeyReleased(nativeKeyEvent: NativeKeyEvent): Unit = {
        if (!isInHotKeys(nativeKeyEvent.getKeyCode)) return
        setHotKeyPressed(nativeKeyEvent.getKeyCode, value = false)
      }

      private def isInHotKeys(code: Int): Boolean = {
        hotKeys.contains(code)
      }

      private def setHotKeyPressed(code: Int, value: Boolean): Unit = {
        pressedHotKeys(code) = value
      }

      private def isAllKeyPressed: Boolean = pressedHotKeys.values.forall(v => v)
    })
  }

  def stopListening(): Unit = {
    try GlobalScreen.unregisterNativeHook()
    catch {
      case e: Exception => e.printStackTrace()
    }
  }
}
