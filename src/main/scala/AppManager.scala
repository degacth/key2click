import java.awt._

import javax.swing._

object AppManager {
  private val frame = new Frame

  try {
    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel")
  } catch {
    case e: Exception => e.printStackTrace()
  }

  UIManager.put("swing.boldMetal", false)

  def create(): Unit = {
    SwingUtilities.invokeLater(() => {
      initTrayMenu()
    })
  }

  def startKey2Click(): Unit = {
    SwingUtilities.invokeLater(() => {
      frame.startAction()
    })
  }

  private def initTrayMenu(): Unit = {
    if (!SystemTray.isSupported) throw new Exception("System tray not supported")

    val trayIcon = new TrayIcon(createImage("images/bulb.gif", "Key 2 Click"))
    val tray = SystemTray.getSystemTray

    tray.add(trayIcon)

    val popup = new PopupMenu()
    val exitItem = new MenuItem("Exit")
    exitItem.addActionListener(_ => {
      tray.remove(trayIcon)
      GlobalKeyListener.stopListening()
      System.exit(0)
    })

    popup.add(exitItem)
    trayIcon.setPopupMenu(popup)
  }

  private def createImage(path: String, description: String): Image = {
    val imageURL = getClass.getResource(path)
    if (imageURL == null) {
      println(s"Resource not found: $path")
      return null
    }

    new ImageIcon(imageURL, description).getImage
  }
}
