import Dependencies._

ThisBuild / scalaVersion := "2.12.8"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"

libraryDependencies += "com.1stleg" % "jnativehook" % "2.1.0"

lazy val root = (project in file("."))
  .settings(
    fork := true,
    name := "key2click",
    libraryDependencies += scalaTest % Test
  )
